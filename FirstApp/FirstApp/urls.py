
from django.contrib import admin

from django.urls import include, path

from django.conf import settings
from django.conf.urls.static import static

from Demo import views

# from Products import views
from django.conf.urls import url


urlpatterns = [


    url(r'^Products/',include('Products.urls')),
    # path('', views.blog_list),

    # path('Demos/', include('Demo.urls')),

    url(r'^admin/', admin.site.urls),
    # path('<id>/delete/',blog_delete),

]