from django.db import models

# Create your models here.

class Product(models.Model):

    Name = models.CharField(max_length = 50)
    Description = models.CharField(max_length=200)
    image_file = models.FileField(upload_to='images')
    Cost = models.FloatField()
    Active = models.CharField(max_length = 10)
    Available = models.CharField(max_length = 10)
    Quantity = models.IntegerField()

    def __str__(self):
        return self.Name