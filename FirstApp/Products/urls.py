from django.conf.urls import url
from django.contrib import admin
from django.urls import path
from .views import Product_List,Product_detail,Product_delete,get_product
from . import views

urlpatterns = [

    # path('',views.Home,name ='Home'),
    url(r'^ProductList/$', views.Product_List,name ='ProductList'),
    url(r'^get_product$', views.get_product,name = 'get_product'),
    url(r'^Productdelete/$', views.Product_delete,name ='Productdelete')
    # path('<id>/',Product_detail),
    # path('<id>/delete/',Product_delete)
]