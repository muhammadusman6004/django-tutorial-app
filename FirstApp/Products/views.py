from django.http import HttpResponseRedirect
from django.shortcuts import render
from Products.models import Product
from .forms import ProductForm
# Create your views here.


def Product_List(request):
    product = Product.objects.all()
    content = {
    'Product_List':product
}
    return render(request,"blog/Product_List.html",content)

def Product_detail(request,id):

    each_product =Product.objects.get(id=id)
    content={

        'Product_detail':each_product
    }
    return render(request,"blog/Product_detail.html",content)

def Product_delete(request):
    id = request.POST.get('delete')
    each_product = Product.objects.get(id=id)
    each_product.delete()
    return HttpResponseRedirect('/Products/ProductList/')


def get_product(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request
        form = ProductForm(request.POST,request.FILES)
        
        # check whether it's valid:
        if form.is_valid():
           form.save() 
           return HttpResponseRedirect('/Products/get_product')   

    # if a GET (or any other method) we'll create a blank form
    else:
        form = ProductForm()

    return render(request, 'blog/Input.html', {'form': form})
