from django.shortcuts import render
from Demo.models import Demo
from django.http import HttpResponseRedirect
from django.http import HttpResponse



# Create your views here.
def blog_list(request):
    demo = Demo.objects.all()
    content = {
    'blog_list':demo
}
    return render(request,"blog/blog_list.html",content)

def blog_detail(request,id):

    each_demo =Demo.objects.get(id=id)
    content={

        'blog_detail':each_demo
    }
    return render(request,"blog/blog_detail.html",content)

def blog_delete(request,id):
    each_demo = Demo.objects.get(id=id)
    each_demo.delete()
    return HttpResponseRedirect('/Demo/')

# def Home(request):
#     return HttpResponse("Hello World")
# def add(request):
#     val1 = int(request.GET["Num1"])
#     val2 = int(request.GET["Num2"])
#     res = val1+val2
#     return render(request,"Home.html",{'result':res})
