from django.db import models
from django.core.files import File
from urllib.request import urlopen
from tempfile import NamedTemporaryFile

# Create your models here.

class Demo(models.Model):
    title = models.CharField(max_length = 200)
    content = models.TextField()

    def __str__(self):
        return self.title


    # ...

    # def get_remote_image(self):
    #     if self.image_url and not self.image_file:
    #        img_temp = NamedTemporaryFile(delete=True)
    #        img_temp.write(urlopen(self.image_url).read())
    #        img_temp.flush()
    #        self.image_file.save(f"image_{self.pk}", File(img_temp))
    #     self.save()